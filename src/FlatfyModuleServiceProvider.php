<?php namespace Visiosoft\FlatfyModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Visiosoft\FlatfyModule\Flatfy\Contract\FlatfyRepositoryInterface;
use Visiosoft\FlatfyModule\Flatfy\FlatfyRepository;
use Anomaly\Streams\Platform\Model\Flatfy\FlatfyFlatfyEntryModel;
use Visiosoft\FlatfyModule\Flatfy\FlatfyModel;
use Illuminate\Routing\Router;

class FlatfyModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/flatfy'           => 'Visiosoft\FlatfyModule\Http\Controller\Admin\FlatfyController@index',
        'admin/flatfy/create'    => 'Visiosoft\FlatfyModule\Http\Controller\Admin\FlatfyController@create',
        'admin/flatfy/edit/{id}' => 'Visiosoft\FlatfyModule\Http\Controller\Admin\FlatfyController@edit',

        '/flatfy' => 'Visiosoft\FlatfyModule\Http\Controller\FlatfyController@createXml',
        '/flatfy/getXml/{id}' => 'Visiosoft\FlatfyModule\Http\Controller\FlatfyController@getXml'
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Visiosoft\FlatfyModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Visiosoft\FlatfyModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Visiosoft\FlatfyModule\Event\ExampleEvent::class => [
        //    Visiosoft\FlatfyModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Visiosoft\FlatfyModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        FlatfyFlatfyEntryModel::class => FlatfyModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        FlatfyRepositoryInterface::class => FlatfyRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
