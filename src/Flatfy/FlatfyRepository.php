<?php namespace Visiosoft\FlatfyModule\Flatfy;

use Visiosoft\FlatfyModule\Flatfy\Contract\FlatfyRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class FlatfyRepository extends EntryRepository implements FlatfyRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var FlatfyModel
     */
    protected $model;

    /**
     * Create a new FlatfyRepository instance.
     *
     * @param FlatfyModel $model
     */
    public function __construct(FlatfyModel $model)
    {
        $this->model = $model;
    }
}
