<?php namespace Visiosoft\FlatfyModule\Flatfy\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface FlatfyRepositoryInterface extends EntryRepositoryInterface
{

}
