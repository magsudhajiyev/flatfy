<?php namespace Visiosoft\FlatfyModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController ;
use Visiosoft\AdvsModule\Adv\Contract\AdvRepositoryInterface;
use Visiosoft\FlatfyModule\Flatfy\Contract\FlatfyRepositoryInterface;
use Anomaly\Streams\Platform\Model\Files\FilesFilesEntryModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class FlatfyController extends PublicController 
{
   
    private $advRepository;
    private $flatfRepository;

    public function __construct(
        AdvRepositoryInterface $advRepository,
        FlatfyRepositoryInterface $flatfyRepository
    )
    {
        parent::__construct();
        $this->advRepository = $advRepository;
        $this->flatfyRepository = $flatfyRepository;
    }


    public function createXml($fields)
    {
        $domtree = new \DOMDocument('1.0', 'UTF-8');

        $announcement = $domtree -> createElement('announcement');
        $announcement = $domtree -> appendChild($announcement);
        


        $currentAd = $domtree->createElement("ad");
        $currentAd = $announcement->appendChild($currentAd);

        
        foreach ($fields as $index => $field) {
            if ($index == 'picture_url') {
                $pictures = $field;
                foreach ($pictures as $picture) {
                    $image = $domtree->createElement('picture');
                    $image->appendChild($domtree->createElement($index, $picture));
                    $currentAd->appendChild($image);
                }
            } else {
                $currentAd->appendChild($domtree->createElement($index, $field));
            }
        }


        dd($domtree->saveXML());

    }

    public function getXml(Request $request, $id)
    {
        $adv = $this->advRepository->find($id);

        $advCF = $adv->cf_json;
        $advCF = json_decode($advCF);

        $flatfyFields = $this->flatfyRepository->all();
        
        foreach ($flatfyFields as $flatfyField) {
            if (!is_null($advCF)) {
                foreach ($advCF as $id => $cFValue) {
                    if ($flatfyField->custom_field_id == substr($id, 2)) {
                        $cFFields[$flatfyField->field_name] = $cFValue;
                    }
                }
            }
            if (!isset($cFFields[$flatfyField->field_name])) {
                $cFFields[$flatfyField->field_name] = null;
            }
        }

        $pictures = DB::table('advs_advs_files')->where('entry_id', $id)->pluck('file_id');
        $images = array();
        foreach ($pictures as $picture) {
            $name = FilesFilesEntryModel::query()->find($picture)->value('name');
            $images[] = $request->root().'/files/images/'.$name;
        }

        $fields = array_merge([
            'Id' => $id,
            'add_time' => $adv->created_at,
            'update_time' => $adv->updated_at,
            'url' => $request->root().'/ad/'.$id,
            'title' => $adv->name,
            'content' => $adv->advs_desc ? $request->advs_desc : 'No Content',
            'price' => $adv->price,
            'address' => $adv->neighborhood,
            'city' => $adv->district,
            'region' => $adv->city,
            'date' => $adv->publish_at,
            'picture_url' => $images,
            'expiration_date' => $adv->finish_at
        ]);

        $xml = $this->createXml($fields);

        dump($xml);

    }
}
