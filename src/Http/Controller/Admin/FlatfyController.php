<?php namespace Visiosoft\FlatfyModule\Http\Controller\Admin;

use Visiosoft\FlatfyModule\Flatfy\Form\FlatfyFormBuilder;
use Visiosoft\FlatfyModule\Flatfy\Table\FlatfyTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class FlatfyController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param FlatfyTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(FlatfyTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param FlatfyFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(FlatfyFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param FlatfyFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(FlatfyFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
