<?php

return [
    'flatfy' => [
        'name'   => 'Flatfy',
        'option' => [
            'read'   => 'Can read flatfy?',
            'write'  => 'Can create/edit flatfy?',
            'delete' => 'Can delete flatfy?',
        ],
    ],
];
