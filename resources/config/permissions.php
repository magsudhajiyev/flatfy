<?php

return [
    'flatfy' => [
        'read',
        'write',
        'delete',
    ],
];
